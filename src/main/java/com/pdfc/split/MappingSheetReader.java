package com.pdfc.split;

import java.io.File;
import java.util.Calendar;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class MappingSheetReader
{
  private XSSFWorkbook workbook = null;
  private XSSFSheet sheet = null;
  private XSSFRow row = null;
  private XSSFCell cell = null;
  
  OPCPackage pkg;
  
  public MappingSheetReader(String path)
  {
    try
    {
      pkg = OPCPackage.open(new File(path));
      workbook = ((XSSFWorkbook)WorkbookFactory.create(pkg));
      sheet = workbook.getSheetAt(0);
      pkg.close();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  public int getRowCount(String sheetName)
  {
    int index = workbook.getSheetIndex(sheetName);
    if (index == -1) {
      return 0;
    }
    sheet = workbook.getSheetAt(index);
    int number = sheet.getLastRowNum() + 1;
    return number;
  }
  
  public synchronized String getCellData(String sheetName, int col_Num, int rowNum)
  {
    try
    {
      if (rowNum <= 0) {
        return "";
      }
      int index = workbook.getSheetIndex(sheetName);
      if (index == -1) {
        return "";
      }
      sheet = workbook.getSheetAt(index);
      row = sheet.getRow(0);
      if (col_Num == -1) {
        return "";
      }
      sheet = workbook.getSheetAt(index);
      row = sheet.getRow(rowNum - 1);
      if (row == null)
        return "";
      cell = row.getCell(col_Num);
      
      if (cell == null) {
        return "";
      }
      if (cell.getCellType() == 1)
        return cell.getStringCellValue();
     
      if ((cell.getCellType() == 0) || (cell.getCellType() == 2)) {
        String cellText = String.valueOf(cell.getNumericCellValue());

        Calendar cal = Calendar.getInstance(); 
 
        if (HSSFDateUtil.isCellDateFormatted(cell)) {
          double d = cell.getNumericCellValue();
          cal.setTime(HSSFDateUtil.getJavaDate(d));
          cellText = String.valueOf(cal.get(1)).substring(2); }
        return cal.get(5) + "/" + cal.get(2) + 1 + "/" + cellText;
      }
      
      if (cell.getCellType() == 3) {
        return "";
      }
      return String.valueOf(cell.getBooleanCellValue());
    }
    catch (Exception e) {
      e.printStackTrace(); }
    return "row " + rowNum + " or column " + col_Num + " does not exist in xls";
  }
  

  public int getColumnCount(String sheetName)
  {
    int index = workbook.getSheetIndex(sheetName);
    if (index == -1) {
      return 0;
    }
    sheet = workbook.getSheet(sheetName);
    row = sheet.getRow(0);
    if (row == null) {
      return -1;
    }
    return row.getLastCellNum();
  }
}
