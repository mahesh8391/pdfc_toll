package com.pdfc.split;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

public class SplitPDF {
    public static Map<String, String> configMap = new HashMap();
    public static String srcDirectory;
    public static String desDirectory;
    public static String resDirectory;
    public static String newDestDirectory;
    public static String newSourceDirectory;
    public static String mapDirectory;
    static MappingSheetReader maptable = null;
    public static int totalRow;
    public static int totalCoumn;
    public static String sourceFile;
    public static String destFile;
    public static String docKey;
    public static int srcStartPage;
    public static int srcEndPage;
    public static int destStartPage;
    public static int destEndPage;
    public static SplitterPDF sp = new SplitterPDF();
    public static int splitterThread;
    public static int caseNumberColPos=0;
    public static int pdfColPos=20;
    public static int startPageColPos=21;
    public static int pageCountColPos=22;

    public SplitPDF() {}

    public static void main(String[] args) throws IOException {
        loadConfiguration();
        loadSourceFiles();
        loadDestinationFiles();
        loadNewSource();
        loadNewDestination();
        //loadMappingsheet();

        //maptable = new MappingSheetReader(mapDirectory);
        //totalRow = maptable.getRowCount("Sheet1");
        //totalCoumn = maptable.getColumnCount("Sheet1");
        //System.out.println("\nTotal no. of Rows Fetched from Mapping sheet : " + (totalRow - 2));
        splitterPDF();
    }

    private static void splitterPDF() throws IOException {
        
    	processBatchFiles(srcDirectory,newSourceDirectory);
    	processBatchFiles(desDirectory, newDestDirectory);
    	
    	
    	
		
    	/*for (int i = 3; i <= totalRow; i++) {
            docKey = maptable.getCellData("Sheet1", 0, i);
            sourceFile = maptable.getCellData("Sheet1", 1, i);
            double d = Double.parseDouble(maptable.getCellData("Sheet1", 2, i));
            srcStartPage = (int) d;
            double d1 = Double.parseDouble(maptable.getCellData("Sheet1", 3, i));
            srcEndPage = (int) d1;
            destFile = maptable.getCellData("Sheet1", 5, i);
            double d2 = Double.parseDouble(maptable.getCellData("Sheet1", 6, i));
            destStartPage = (int) d2;
            double d3 = Double.parseDouble(maptable.getCellData("Sheet1", 7, i));
            destEndPage = (int) d3;
            try {
                sp.multiPDFSplit(docKey, "Source", srcDirectory, newSourceDirectory, sourceFile,
                        srcStartPage, srcEndPage);
                sp.multiPDFSplit(docKey, "Destination", desDirectory, newDestDirectory, destFile,
                        destStartPage, destEndPage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
        System.out.println("\nPDF Splitting completed..!");
        System.out.println("Source PDF are saved in new Path :" + newSourceDirectory);
        System.out.println("Destination PDF are saved in new Path :" + newDestDirectory);
    }
    
    private static void processBatchFiles(String srcDirectory,String newSrcDirectory) throws FileNotFoundException
    {
    	File[] batchFiles = new File(srcDirectory).listFiles();
		
		for(File file:batchFiles)
		{
			
			if(file.getAbsolutePath().toLowerCase().endsWith("txt"))
			{
				//processASCIIFile(recordMap,file.getAbsolutePath());
				File batchFile=new File(file.getAbsolutePath());
				Scanner sc=new Scanner(batchFile);
				int lineCount=0;
				
				while(sc.hasNextLine()) 
				{
					String recordString=sc.nextLine();
					if(lineCount==0)
					{
						lineCount++;
						continue;
						
					}
					
					String recordStrArr[]=recordString.split("\\|");
					
					String docKey=recordStrArr[caseNumberColPos];
					String sourceFile=recordStrArr[pdfColPos];
					int srcStartPage=Integer.parseInt(recordStrArr[startPageColPos]);
					int srcEndPage=Integer.parseInt(recordStrArr[pageCountColPos]);
					srcEndPage+=srcStartPage-1;
					
					
					try 
					{
		                sp.multiPDFSplit(docKey,srcDirectory, newSrcDirectory, sourceFile,
		                        srcStartPage, srcEndPage);
					}catch (IOException e) {
		                e.printStackTrace();
		            }  
				}	
					
			}
			
		}
    }

    private static void loadSourceFiles() {
        srcDirectory = (String) configMap.get("sourceDirectory");
        if (new File(srcDirectory).exists()) {
            System.out.println("\nSource PDF Fetched from  : " + srcDirectory);
        } else
            System.out.println(
                    "Old Source Folder Doesn't exist..! Please provide correct Source folder path");
    }

    private static void loadDestinationFiles() {
        desDirectory = (String) configMap.get("destinationDirectory");
        if (new File(desDirectory).exists()) {

            System.out.println("\nDestination PDF Fetched from  : " + desDirectory);
        } else
            System.out.println(
                    "Old Destination Folder Doesn't exist..! Please provide correct Destination folder path");
    }

    private static void loadNewSource() {
        newSourceDirectory = (String) configMap.get("newSourceDirectory");
        if (!new File(newSourceDirectory).exists()) {

            new File(newSourceDirectory).mkdir();
            System.out.println("New Source : " + newSourceDirectory + " Created!");
        }
    }

    private static void loadNewDestination() {
        newDestDirectory = (String) configMap.get("newDestDirectory");
        if (!new File(newDestDirectory).exists()) {

            new File(newDestDirectory).mkdir();
            System.out.println("New Destination : " + newDestDirectory + "Created");
        }
    }

    private static void loadMappingsheet() {
        mapDirectory = (String) configMap.get("mappingsheet");
        System.out.println("\nLoading Mapping sheet from : " + mapDirectory);
    }

    private static void loadConfiguration() {
        if (new File("config.properties").exists()) {
            System.out.println("Configuration file exists..! Loading Configurations..!");
            Properties prop = new Properties();
            try {
                prop.load(new FileInputStream("config.properties"));
                for (String key : prop.stringPropertyNames()) {
                    configMap.put(key, prop.getProperty(key));
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                System.out.println("config. file does not exist..!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
