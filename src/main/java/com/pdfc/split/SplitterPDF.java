package com.pdfc.split;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;

public class SplitterPDF
{
  File file;
  
  SplitterPDF() {}
  
  public void multiPDFSplit(String dKey,String inPath, String outPath, String fileName, int stPage, int enPage) throws java.io.IOException
  {
    file = new File(inPath + "/" + fileName);
    PDDocument document = PDDocument.load(file);
    Splitter splitter = new Splitter();
    splitter.setStartPage(stPage);
    splitter.setEndPage(enPage);
    List<PDDocument> pd = splitter.split(document);
    Iterator<PDDocument> iterator = pd.listIterator();
    PDDocument endDoc = new PDDocument();
    Iterator localIterator;
    for (; iterator.hasNext();localIterator.hasNext())
    {
      PDDocument splitpdf = (PDDocument)iterator.next();
      localIterator = splitpdf.getPages().iterator(); 
//      continue;
      PDPage pg = (PDPage)localIterator.next();
      endDoc.addPage(pg);
    }
   
    endDoc.save(outPath + "/" + dKey + ".PDF");
    
    document.close();
    endDoc.close();
  }
}

